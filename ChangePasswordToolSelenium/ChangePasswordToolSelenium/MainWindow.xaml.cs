﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ChangePasswordToolSelenium
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #region Properties
        private string _CaptchaKey = "25fbc812b40b10686f4dc98105bbf62f";
        public string CaptchaKey { get => _CaptchaKey; set { _CaptchaKey = value; OnPropertyChanged(); } }
        private string _Password = "PasswordAsdf123@";
        public string Password { get => _Password; set { _Password = value; OnPropertyChanged(); } }
        private string _Log;
        public string Log { get => _Log; set { _Log = value; OnPropertyChanged(); } }
        private string _DelayTime = "2";
        public string DelayTime { get => _DelayTime; set { _DelayTime = value; OnPropertyChanged(); } }
        public List<Model> ListAccs { get; set; } = new List<Model>();

        string datasitekey = "6LdaGwcTAAAAAJfb0xQdr3FqU4ZzfAc_QZvIPby5";
        #endregion

        #region Class
        public class Model
        {
            public string Link { get; set; }
            public bool Status { get; set; }
            public int Id { get; set; }
        }
        #endregion



        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void WriteLog(string log)
        {
            Log = log + '\n' + Log;
        }
        private void WriteToFile(string filename, string body)
        {
            if (!System.IO.File.Exists(filename))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(filename))
                {
                    sw.WriteLine(body);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(filename))
                {
                    sw.WriteLine(body);
                }
            }
        }

        private void LoadFile()
        {
            int i = 1;
            var listLink = System.IO.File.ReadAllLines("ee.txt");
            foreach (var item in listLink)
            {
                ListAccs.Add(new Model { Id = i, Link = item, Status = false });
                i++;
            }
        }

        private void StartClick(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                WriteLog("Bắt đầu đọc file");
                LoadFile();
                WriteLog("Kết thúc đọc file");

                Thread t = new Thread(() =>
                {
                    foreach (var item in ListAccs)
                    {
                        if (item?.Link != null)
                        {
                            bool isOK = DoSomething(item);
                            if (isOK)
                            {
                                WriteLog("Success acc " + item.Id);
                                //System.IO.File.WriteAllText("Success.txt", $"Success acc {item.Link}\n");
                                WriteToFile("Success.txt", $"Success acc {item.Link}");
                            }
                            else
                            {
                                WriteLog("Failed acc " + item.Id);
                                //System.IO.File.WriteAllText("Failed.txt", $"Failed acc {item.Link}\n");
                                WriteToFile("Failed.txt", $"Failed acc {item.Link}");
                            }
                            Thread.Sleep(TimeSpan.FromSeconds(Convert.ToInt32(DelayTime)));
                        }
                    }
                    MessageBox.Show("Hoàn tất", "Thông báo");
                });

                t.Start();
            }
        }


        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(DelayTime))
            {
                MessageBox.Show("DelayTime không được trống!", "Cảnh báo");
                return false;
            }
            if (string.IsNullOrEmpty(CaptchaKey))
            {
                MessageBox.Show("CaptchaKey không được trống!", "Cảnh báo");
                return false;
            }
            if (string.IsNullOrEmpty(Password))
            {
                MessageBox.Show("Password không được trống!", "Cảnh báo");
                return false;
            }
            //if (DateTime.Now > (new DateTime(2020, 11, 27)).AddDays(7))
            //{
            //    this.Close();
            //    return false;
            //}
            return true;
        }

        private bool DoSomething(Model item)
        {
            WriteLog("Bắt đầu thread " + item.Id);
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("no-sandbox");
            //options.AddArgument("headless");
            ChromeDriver chromeDriver = new ChromeDriver(@"D:\NA\Downloads\chromedriver_win32", options);
            chromeDriver.Url = item.Link;
            chromeDriver.Navigate();

            WriteLog("Bắt đầu tìm form_new_password " + item.Id);
            IWebElement form_new_password = null;
            try
            {
                form_new_password = chromeDriver.FindElementById("form_new_password");
            }
            catch (Exception)
            {
            }
            if (form_new_password != null)
            {
                WriteLog("Tìm được form_new_password " + item.Id);
                //form_new_password.Click();
                try
                {
                    form_new_password.SendKeys(Password);
                }
                catch (Exception)
                {
                }
                WriteLog("Send key xong form_new_password " + item.Id);
            }
            else
            {
                WriteLog("Không tìm được form_new_password " + item.Id);
                return false;
            }

            WriteLog("Bắt đầu tìm form_check_password " + item.Id);
            IWebElement form_check_password = null;
            try
            {
                form_check_password = chromeDriver.FindElementById("form_check_password");
            }
            catch (Exception)
            {
            }
            if (form_check_password != null)
            {
                WriteLog("Tìm được form_check_password " + item.Id);
                //form_check_password.Click();
                try
                {
                    form_check_password.SendKeys(Password);
                }
                catch (Exception)
                {
                }
                WriteLog("Send key xong form_check_password " + item.Id);
            }
            else
            {
                WriteLog("Không tìm được form_check_password" + item.Id);
                return false;
            }

            WriteLog("Bắt đầu giải captcha" + item.Id);
            string s;
            bool isCaptchaSolved = SolveRecaptchaV2(datasitekey, item.Link, out s);
            if (isCaptchaSolved)
            {
                WriteLog("Giải captcha thành công " + item.Id + "s = " + s);
                string jsString = $"document.getElementById('g-recaptcha-response').innerHTML=\"{s}\";";
                // thực thi JavaScript dùng IJavaScriptExecutor
                IJavaScriptExecutor js = chromeDriver as IJavaScriptExecutor;
                // javascript cần return giá trị.
                var dataFromJS = (string)js.ExecuteScript(jsString);
                WriteLog("Kết quả nhập captcha " + item.Id + "kết quả = " + dataFromJS);
            }
            else
            {
                WriteLog("Giải captcha thất bại " + item.Id);
                return false;
            }

            //form_send

            WriteLog("Bắt đầu click button submit " + item.Id);
            IWebElement form_send = null;
            try
            {
                form_send = chromeDriver.FindElementById("form_send");
            }
            catch (Exception)
            {
            }
            if (form_send != null)
            {
                WriteLog("Tìm được form_send " + item.Id);
                //form_check_password.Click();
                try
                {
                    form_send.Click();
                }
                catch (Exception)
                {
                }
                WriteLog("Click xong form_send " + item.Id);
            }
            else
            {
                WriteLog("Không tìm được form_send" + item.Id);
                return false;
            }

            Thread.Sleep(TimeSpan.FromSeconds(2));
            chromeDriver.Quit();
            return true;
        }

        public bool SolveRecaptchaV2(string googleKey, string pageUrl, out string result)
        {
            string requestUrl = string.Concat(new string[]
            {
                "http://2captcha.com/in.php?key=",
                this.CaptchaKey,
                "&method=userrecaptcha&googlekey=",
                googleKey,
                "&pageurl=",
                pageUrl
            });
            bool result2;
            try
            {
                WebRequest req = WebRequest.Create(requestUrl);
                using (WebResponse resp = req.GetResponse())
                {
                    using (StreamReader read = new StreamReader(resp.GetResponseStream()))
                    {
                        string response = read.ReadToEnd();
                        bool flag = response.Length < 3;
                        if (flag)
                        {
                            result = response;
                            result2 = false;
                            return result2;
                        }
                        bool flag2 = response.Substring(0, 3) == "OK|";
                        if (flag2)
                        {
                            string captchaID = response.Remove(0, 3);
                            for (int i = 0; i < 24; i++)
                            {
                                WebRequest getAnswer = WebRequest.Create("http://2captcha.com/res.php?key=" + this.CaptchaKey + "&action=get&id=" + captchaID);
                                using (WebResponse answerResp = getAnswer.GetResponse())
                                {
                                    using (StreamReader answerStream = new StreamReader(answerResp.GetResponseStream()))
                                    {
                                        string answerResponse = answerStream.ReadToEnd();
                                        bool flag3 = answerResponse.Length < 3;
                                        if (flag3)
                                        {
                                            result = answerResponse;
                                            result2 = false;
                                            return result2;
                                        }
                                        bool flag4 = answerResponse.Substring(0, 3) == "OK|";
                                        if (flag4)
                                        {
                                            result = answerResponse.Remove(0, 3);
                                            result2 = true;
                                            return result2;
                                        }
                                        bool flag5 = answerResponse != "CAPCHA_NOT_READY";
                                        if (flag5)
                                        {
                                            result = answerResponse;
                                            result2 = false;
                                            return result2;
                                        }
                                    }
                                }
                                Thread.Sleep(5000);
                            }
                            result = "Timeout";
                            result2 = false;
                            return result2;
                        }
                        result = response;
                        result2 = false;
                        return result2;
                    }
                }
            }
            catch
            {
            }
            result = "Unknown error";
            result2 = false;
            return result2;
        }
    }
}
